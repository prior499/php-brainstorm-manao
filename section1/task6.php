<?php 
function task6() {
	
	for($i = 1000; $i <= 9999; $i++){
		$match = false;
		$num = $i;
		$testNum = 2370;
		$attempt = 4;
		while($testNum != 0 && $attempt != 0) {
			$testLastDigit = $testNum % 10;
			$lastDigit = $num % 10;
			if($lastDigit == $testLastDigit) {
				$match = true;
				$testNum = ($testNum - $testLastDigit) / 10;
				$num = $i;
				$attempt = 4;
			} else {
				$attempt--;
				$match = false;
				$num = ($num - $lastDigit) / 10;
			}
		}
		if($match) 
			echo $i.' ';
	}
}
echo task6();