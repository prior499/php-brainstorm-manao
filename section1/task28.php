<?php 
function countDigit($num) {
	$num = (int)$num;
	$count = 0;
	while($num != 0) {
		$lastDigit = $num % 10;
		$num = ($num - $lastDigit) / 10;
		$count++;
	}
	return $count;
}
function getMaxDigit($num) {
	$numTest = $num;
	$maxDigit = $numTest % 10;;
	while($numTest != 0) {
		$lastDigit = $numTest % 10;
		$numTest = ($numTest - $lastDigit) / 10;
		if($maxDigit < $lastDigit) {
			$maxDigit = $lastDigit;
		}
	}
	return $maxDigit;
} 
function task($num1, $num2) {

	for($i = $num1; $i <= $num2; $i++) {
		$firstNumber = $i;
		$secondNumber = 0;
		$descNumber = 0;
		
		$countCicle = countDigit($i);
		while($countCicle != 0) {
			$maxDigit = getMaxDigit($firstNumber);
			$descNumber *= 10;
			$descNumber += $maxDigit;

			$countCicle--;
			$countSelectDigit = 0;

			$secondNumber = 0;
			$lastDigit = $firstNumber % 10;
			if($lastDigit == 0 && $secondNumber == 0) {
				$secondNumber = 1;
			}
			while($firstNumber != 0) {
				$lastDigit = $firstNumber % 10;
				$firstNumber = ($firstNumber - $lastDigit) / 10;
				if($lastDigit != $maxDigit || $countSelectDigit == 1) {
					$secondNumber *= 10;
					$secondNumber += $lastDigit;
				} else if($countSelectDigit == 0) {
					$countSelectDigit++;
				}
			}
			$firstNumber = $secondNumber;
		}
		echo $descNumber;
		echo "<br>";
	}
}

task(43, 324);