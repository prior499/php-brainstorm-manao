<?php
function task($firstNumber, $endNumber) {
	$sumDivide = 0;
	$maxSumDivide = 0;
	$resultNum = 0;
	while($firstNumber <= $endNumber) {
		for ($i = 1; $i <= $firstNumber; $i++) {
	        if ($firstNumber % $i == 0) {
	            $sumDivide += $i;

	        }
		} 
		if($maxSumDivide < $sumDivide) {
			$maxSumDivide = $sumDivide;
			$resultNum = $firstNumber;
		}
		$sumDivide = 0;
		$firstNumber++;

	}

	echo $resultNum;
}

task(123, 435);