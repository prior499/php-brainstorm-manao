<?php 
function countDigit($num) {
	$num = (int)$num;
	$count = 0;
	while($num != 0) {
		$lastDigit = $num % 10;
		$num = ($num - $lastDigit) / 10;
		$count++;
	}
	return $count;
}

function getMinDigit($num) {
	$numTest = $num;
	$minDigit = $numTest % 10;;
	while($numTest != 0) {
		$lastDigit = $numTest % 10;
		$numTest = ($numTest - $lastDigit) / 10;
		if($minDigit > $lastDigit) {
			$minDigit = $lastDigit;
		}
	}
	return $minDigit;
} 

function task($num) {
	$firstNumber = $num;
	$secondNumber = 0;
	$ascNumber = 0;
	
	$countCicle = countDigit($num);
	while($countCicle != 0) {
		$minDigit = getMinDigit($firstNumber);
		if($minDigit == 0 && $ascNumber == 0) {
			$ascNumber = 1;
		}
		$ascNumber *= 10;
		$ascNumber += $minDigit;

		$countCicle--;
		$countSelectDigit = 0;

		$secondNumber = 0;
		$lastDigit = $firstNumber % 10;
		if($lastDigit == 0 && $secondNumber == 0) {
			$secondNumber = 1;
		}
		while($firstNumber != 0) {
			$lastDigit = $firstNumber % 10;
			$firstNumber = ($firstNumber - $lastDigit) / 10;
			if($lastDigit != $minDigit || $countSelectDigit == 1) {
				$secondNumber *= 10;
				$secondNumber += $lastDigit;
			} else if($countSelectDigit == 0) {
				$countSelectDigit++;
			}
		}
		$firstNumber = $secondNumber;
	}
	echo $ascNumber;
}
task(958674213);