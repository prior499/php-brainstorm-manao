<?php 
function task3($num) {
	$result = ' ';
	while($num != 0) {
		$lastDigit = $num % 10;
		$num = ($num - $lastDigit) / 10;
		$prevDigit = $num % 10;
		if($lastDigit > $prevDigit) {
            $result = 'По возрастанию';
		} else {
			$result = 'Не по возрастанию';
		    break;
        }
	}
	echo $result;
}
task3(123400);