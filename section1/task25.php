<?php
function factorial ($num) {
    $f = 1;
    if ($num != 0){
       for ($i = 1; $i <= $num ; $i++) { 
          $f = $f*$i;
       }
    }
    return $f;
}

function task($firstNumber, $secondNumber) {
	$sumNumber = 0;
	$countNumber = 0;
	for($i = $firstNumber; $i <= $secondNumber; $i++) {
		$num = $i;
		$sumFactorial = 0;
		while($num != 0) {
			$lastDigit = $num % 10;
			$num = ($num - $lastDigit) / 10;
			$sumFactorial += factorial($lastDigit);
		}
		if($i == $sumFactorial) {
			$sumNumber += $i;
			$countNumber++;
		}
	}	
	echo "Сумма чисел факториалов-".$sumNumber." Количество чисел-".$countNumber;
}

task(1, 1000);