<?php

function countDigit($num) {
    $num = (int)$num;
    $count = 0;
    while($num != 0) {
        $lastDigit = $num % 10;
        $num = ($num - $lastDigit) / 10;
        $count++;
    }
    return $count;
}

function checkBySum($num, $sum, $countCycle = 0) {
    $testNum = $num;
    $leftSum = $sum;
    while ($testNum != 0 && $countCycle > 1) {
        $lastDigit = $testNum % 10;
        $testNum = ($testNum - $lastDigit) / 10;
        $leftSum -= $lastDigit;
        if($leftSum != 0) {
            if($leftSum < 0) {
                $leftSum += $lastDigit;
            } else {
                $countCycle--;
                if(checkBySum($testNum, $leftSum, $countCycle)) {
                    return true;
                } else {
                    return false;
                }
            }
        } else {
            return true;
        }
    }
    return false;
}

function task($num, $lastNum, $terms) {
    for($i = $num; $i <= $lastNum; $i++) {
        $countCycle = countDigit($i);
        if(checkBySum($i, $terms, $countCycle)) {
            echo $i;
            echo '</br>';
        }
    }
}

task(121, 176, 8);