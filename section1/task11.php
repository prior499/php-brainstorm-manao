<?php
function task($num1, $num2) {
	for ($i = $num1; $i <= $num2; $i++) {
		$numTest = $i;
		$sumDigit = 0;
		while($numTest != 0) {
			$lastDigit = $numTest % 10;
			$numTest = ($numTest - $lastDigit) / 10;
			$sumDigit += $lastDigit;
		}
		if($i % $sumDigit == 0) {
			echo $i.' ';
		}
	}
}

task(12, 135);