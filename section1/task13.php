<?php 

function countDigit($num) {
	$num = (int)$num;
	$count = 0;
	while($num != 0) {
		$lastDigit = $num % 10;
		$num = ($num - $lastDigit) / 10;
		$count++;
	}
	return $count;
}

function task($num) {
	$firstNumber = $num;
	$secondNumber = 0;
	$countDigit = countDigit($num);

	if($countDigit % 2 == 0) {
		$i = $countDigit / 2;

		while($i != 0) {
			$lastDigit = $firstNumber % 10;
			$firstNumber = ($firstNumber - $lastDigit) / 10;
			if($secondNumber == 0 && $lastDigit == 0)
					$secondNumber = 1;
			$secondNumber *= 10;
			$secondNumber += $lastDigit;
			$i--;
		}
		if($firstNumber == $secondNumber) {
			echo "m симметрично";
		}
	}
}
task(34655643);