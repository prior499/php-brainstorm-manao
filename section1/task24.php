<?php
function reversNumber($num) {
	$firstNumber = $num;
	$reversNumber = 0;
	
	while($firstNumber != 0) {
		$lastDigit = $firstNumber % 10;
		$firstNumber = ($firstNumber - $lastDigit) / 10;
		if($reversNumber == 0 && $lastDigit == 0)
				$reversNumber = 1;
		$reversNumber *= 10;
		$reversNumber += $lastDigit;
	}
	return $reversNumber;
}

function task($num) {
	$firstNumber = $num;
	$secondNumber = $num;
	$binNumber = 0;
	$hexNumber = ' ';
	while($firstNumber != 0) {
		$residue = $firstNumber % 2;
		if($residue != 0) {
			$firstNumber = $firstNumber - 1;
		}
		$firstNumber = $firstNumber / 2;
		$binNumber *= 10;
		$binNumber += $residue;
	}
	$binNumber = reversNumber($binNumber);
	echo 'В двоичной: '.$binNumber;
	echo '<br>';

	while($secondNumber != 0) {
		$residue = $secondNumber % 16;
		if($residue != 0) {
			$secondNumber = $secondNumber - $residue;
		}
		switch ($residue) {
			case '10': $residue = 'A'; break;
			case '11': $residue = 'B'; break;
			case '12': $residue = 'C'; break;
			case '13': $residue = 'D'; break;
			case '14': $residue = 'E'; break;
			case '15': $residue = 'F'; break;
		}
		$secondNumber = $secondNumber / 16;
		$hexNumber .= $residue;
	}
	$hexNumber = strrev($hexNumber);
	echo 'В шестнадцатиричной: '.$hexNumber;
}

task(1234);