<?php 
function task($num)
{
	for($i = $num; $i > 10; $i--) {

		$firstNumber = $i;
		$secondNumber = 0;
		
		while($firstNumber != 0) {
			$lastDigit = $firstNumber % 10;
			$firstNumber = ($firstNumber - $lastDigit) / 10;
			if($secondNumber == 0 && $lastDigit == 0)
				$secondNumber = 1;
			$secondNumber *= 10;
			$secondNumber += $lastDigit;
		}
		if($i == $secondNumber) {
			$numSqr = $secondNumber * $secondNumber;
			$firstNumSqr = $numSqr;
			$secondNumSqr = 0;
			while($firstNumSqr != 0) {
				$lastDigit = $firstNumSqr % 10;
				$firstNumSqr = ($firstNumSqr - $lastDigit) / 10;
				$secondNumSqr *= 10;
				$secondNumSqr += $lastDigit;
			}
			if($numSqr == $secondNumSqr) {
				echo $i.' ';
				echo $secondNumSqr.' ';
			}
		}
	}
}

task(1000);