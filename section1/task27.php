<?php
function isPrimeNum($num) {
	$result = '';
	$sqrtNum = $num;
	for ($x = 0; $x < 10; ++$x) {
        $sqrtNum = ($sqrtNum + $num / $sqrtNum) / 2;
    }
	for($i=2; $i <= $sqrtNum; $i++) {
        if($num % $i == 0) {
            return false;
        }
    }
    return true;
}

function task($firstNumber, $endNumber) {
	$sumPrimeDivide = 0;
	$maxSumPrimeDivide = 0;
	$resultNum = 0;
	while($firstNumber <= $endNumber) {
		for ($i = 1; $i <= $firstNumber; $i++) {
	        if ($firstNumber % $i == 0) {
	        	if(isPrimeNum($i)) {
	        		$sumPrimeDivide += $i;
	        	}
	        }
		} 
		if($maxSumPrimeDivide < $sumPrimeDivide) {
			$maxSumPrimeDivide = $sumPrimeDivide;
			$resultNum = $firstNumber;
		}
		$sumPrimeDivide = 0;
		$firstNumber++;
	}

	echo $resultNum;
}

task(123, 435);