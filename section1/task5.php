<?php 
function task5($num) {
	$firstNumber = $num;
	$secondNumber = 0;
	
	while($firstNumber != 0) {
		$lastDigit = $firstNumber % 10;
		$firstNumber = ($firstNumber - $lastDigit) / 10;
		if($secondNumber == 0 && $lastDigit == 0)
				$secondNumber = 1;
		$secondNumber *= 10;
		$secondNumber += $lastDigit;
	}
}
task5(76543);