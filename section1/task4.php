<?php 
function task() {
    for($i = 1000; $i < 9999; $i += 2) {
        $num = $i;
        $result = ' ';
        $sort = 0;
        while($num != 0) {
            $lastDigit = $num % 10;
            $num = ($num - $lastDigit) / 10;
            $prevDigit = $num % 10;
            if($lastDigit > $prevDigit) {
                if($sort == 91) {
                    $sort = 0;
                    break;
                }
                $sort = 19;
            } else if($lastDigit < $prevDigit) {
                if($sort == 19) {
                    $sort = 0;
                    break;
                }
                $sort = 91;
            } else {
                $sort = 0;
                break;
            }
        }
        if($sort != 0) {
            echo $i.' ';
        }
    }
}
task();
