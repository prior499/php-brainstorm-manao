<?php 
function task() {
	for($i = 1000; $i <= 9999; $i++) {
		$num = $i;
		$storageNum = 0;
		$doubleDigit = false;
		while($num != 0) {
			$lastDigit = $num % 10;
			$num = ($num - $lastDigit) / 10;
			while($testStorageNum != 0) {
				$storageLastDigit = $testStorageNum % 10;
				$testStorageNum = ($testStorageNum - $storageLastDigit) / 10;
				if($storageLastDigit == $lastDigit) {
					$doubleDigit = true;
					break 2;
				} 
			} 
			if($lastDigit == 0 && $storageNum == 0) {
				$lastDigit = $num % 10;
				$num = ($num - $lastDigit) / 10;
				if($lastDigit == 0) {
					$doubleDigit = true;
					break;
				} else {
					$storageNum = $lastDigit * 10;
					$testStorageNum = $storageNum;
					continue;
				}
			}
			$storageNum *= 10;
			$storageNum += $lastDigit;
			$testStorageNum = $storageNum;
		}
		if(!$doubleDigit) {
			echo $i." ";
		}
	}
}
task();