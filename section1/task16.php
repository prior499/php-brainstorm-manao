<?php
function task($num1, $num2) {
	
	for ($numTest = $num1; $numTest <= $num2; $numTest++) {
		$sumDelimiter = 0;
		for ($x = 1; $x <= $numTest/2; $x++) {
	        if ($numTest % $x == 0) {
	            $sumDelimiter += $x;
	        }
		}
		if($sumDelimiter == $numTest) {
	        echo $numTest.' ';
		}
	}
}

task(1, 500);