<?php 
function task($num1, $num2) {
	$sumDelimiterMax = 0;
	$numMaxDelimiter = 0;
	for ($numTest = $num1; $numTest <= $num2; $numTest++) {
		$sumDelimiter = 0;
		for ($x = 1; $x <= $numTest/2; $x++) {
	        if ($numTest % $x == 0) {
	            $sumDelimiter += $x;
	        }
		}
		if($sumDelimiter > $sumDelimiterMax) {
			$sumDelimiterMax = $sumDelimiter;

			$numMaxDelimiter = $numTest;
		}
	}
	echo $numMaxDelimiter;
}

task(12, 100);