<?php 
function task($num)
{
	$result = 0;
	$sqrNum = $num * $num;
	while($num != 0) {
		$lastDigit = $num % 10;
		$num = ($num - $lastDigit) / 10;
		$lastDigitSqrt = $sqrNum % 10;
		$sqrNum = ($sqrNum - $lastDigitSqrt) / 10;
		
		if($lastDigit != $lastDigitSqrt) {
			$result = 0;
			break;
		}
		
		$result = 1;
	} 
    return $result;
}

echo task(626);
