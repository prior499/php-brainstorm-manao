<?php 

/*В массиве А(N) максимальные элементы, являющиеся четными числами, заменить значениями их индексов.*/

function task($arrA) {
	$maxElem = array();
	foreach ($arrA as $value) {
		if(empty($maxElem)) { 
			$maxElem = $value;
		}
		if($maxElem < $value) {  
			if($value % 2 == 0)
				$maxElem = $value;
		} 
	}
	foreach ($arrA as $key => $value) {
		if($maxElem == $value) {
			$arrA[$key] = $key;
		} 
	}
	return $arrA;
}

$arr = array();
for($i = 0; $i < 20; $i++)
	$arr[] = rand(-3, 10);
var_dump($arr);

var_dump(task($arr));