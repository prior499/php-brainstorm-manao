<?php 

/*
40. ��������   �������������   ��   �����������   ������   �(�)   ����� ������� ���� ������������ �� ����������� �������� �(N) � �(�), ��� �=�+N.

*/

class positioningElements
{
	public $arrResult;
	public $arrFirst;
	public $arrSecond;

	public function getAscArr($arr) {
		$result = array();
		$countElem = count($arr); 
		for ($i = 0; $i < $countElem; $i++) {
			$elem = $this->getMinElem($arr);
			$result[$elem['key']] = $elem['value'];
			unset( $arr[ $elem['key'] ] );
		}
		return $result;
	}

	public function getMinElem($arr) {
		$minElem = false;
		foreach ($arr as $key => $val) {
			if($val < $minElem['value'] || $minElem === false) 
				$minElem = array('key' => $key, 'value' => $val);
		}
		return $minElem;
	}

	public function getArray($countElem = 10, $rangeMin = -10, $rangeMax = 10) {
		$arr = array();
		for($i = 0; $i < $countElem; $i++)
			$arr[] = rand($rangeMin, $rangeMax);
		return $arr;
	}

	public function task() {
		$countElems = count($this->arrFirst);
		for ($i = 0; $i < $countElems; $i++) {
			$elemFirst = current($this->arrFirst);
			$elemSecond = current($this->arrSecond);
			$this->arrResult[] = $elemFirst + $elemSecond;
			next($this->arrFirst);
			next($this->arrSecond);
		}
		$this->arrResult = $this->getAscArr($this->arrResult);
		return $this->arrResult;
	}

}

$task = new positioningElements($arr);

$task->arrFirst = $task->getAscArr($task->getArray(10, 1, 20));
$task->arrSecond = $task->getAscArr($task->getArray(10, 1, 20));

echo "<pre> Array 1: <br>";
print_r($task->arrFirst);
echo "<br> Array 2: <br>";
print_r($task->arrSecond);
echo "<br>Task: <br>";
print_r($task->task());
echo "</pre>";


