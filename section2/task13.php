<?php 

/*В  массиве  А(N)  найти  два  наименьших  элемента  и  два  наибольших элемента.  */


function findMaxElem($arr, $count) {	
	$elems = array();
	while ($count != 0) {
		$elemKey = 0;
		$elem = 0;
	 	foreach ($arr as $key => $value) {
			if($elem < $value || empty($elem)) {
				$elemKey = $key;
				$elem = $value;
			} 
		}
		unset($arr[$elemKey]);
		$elems[] = $elem;
	 	$count--;
	}
	return $elems;
}
function findMinElem($arr, $count) {	
	$elems = array();
	while ($count != 0) {
		$elemKey = 0;
		$elem = 0;
	 	foreach ($arr as $key => $value) {
			if($elem > $value || empty($elem)) {
				$elemKey = $key;
				$elem = $value;
			} 
		}
		unset($arr[$elemKey]);
		$elems[] = $elem;
	 	$count--;
	}
	return $elems;
}

function task($arr, $count = 2) {
 	$elems["MAX"] = findMaxElem($arr, $count);
 	$elems["MIN"] = findMinElem($arr, $count);
	return $elems;
}

$arr = array();
for($i = 0; $i < 30; $i++)
	$arr[] = rand(-30, 10);
var_dump($arr);

print_r(task($arr));