<?php 

/*В массиве А(N) поменять местами последний отрицательный элемент с максимальным элементом. */

function task($arrA) {
	$maxElem = array();
	$lastNegativeElem = array();

	foreach ($arrA as $key => $value) {

		if($maxElem['value'] < $value || empty($maxElem)) 
			$maxElem = array('key' => $key, 'value' => $value);
		if($value < 0) 
			$lastNegativeElem = array('key' => $key, 'value' => $value);
	}
	$arrA[$lastNegativeElem['key']] = $maxElem['value'];
	$arrA[$maxElem['key']] = $lastNegativeElem['value'];
	return $arrA;
}

$arr = range(-2, 5);
shuffle($arr);
var_dump($arr);

var_dump(task($arr));