<?php 

/*
34. � ������� �(N) ����������� �������� � ������� �����������, ����� �������� � ���� ����� k �����, �� ������� ���������������.

*/

class positioningElements
{
	public $arr;
	public $result;
	public $resultSecond;

	public function getArray($countElem = 10) {
		$arr = array();
		for($i = 0; $i < $countElem; $i++)
			$arr[] = rand(-10, 10);
		return $arr;
	}

	public function maxElem($arr) {
		$maxElem = false;
		foreach ($arr as $val) {
			if($val > $maxElem || $maxElem === false) $maxElem = $val;
		}
		return $maxElem;
	}
	public function minElem($arr) {
		$minElem = false;
		foreach ($arr as $val) {
			if($val < $minElem || $minElem === false) $minElem = $val;
		}
		return $minElem;
	}

	private function getAscElem() {
		$elem = false;
		foreach ($this->arr as $key => $val) {
			if( $elem === false || $val < $elem ) {
				$elemKey = $key;
				$elem = $val;
			}
		}
		unset($this->arr[$elemKey]);
		return $elem;
	}

	public function getInsertNum($arr, $firstRangeNum = false, $lastRangeNum = false) {
		$result = array();
		$firstNum = $firstRangeNum;
		$rangeMaxNum = $lastRangeNum;
		if( $firstRangeNum === false) {
			$firstRangeNum = $this->minElem($arr);
			if($firstRangeNum <= $lastRangeNum)
				$result[] = $firstRangeNum;
		}
		if( $lastRangeNum === false)
			$lastRangeNum = $this->maxElem($arr);

		foreach($arr as $val) {
			if($val > $firstRangeNum && $val <= $lastRangeNum)
				$result[] = $val;
		}
		
		sort($result);
		return $result;
	}

	public function task($arr) {
		$this->arr = $arr;
		$this->result = array();
		$i = count($arr);
		while ($i > 0) {
			$i--;
			$this->result[] = $this->getAscElem();
		}
		return $this->result;
	}

	public function taskSecond($arrInsert) {
		$valPrev = false;
		$this->resultSecond = array();
		foreach ($this->result as $valA) {
			foreach ($this->getInsertNum($arrInsert, $valPrev, $valA) as $valB) {
				$this->resultSecond[] = $valB;
			}
			$this->resultSecond[] = $valA;
			$valPrev = $valA;
		}
		foreach ($this->getInsertNum($arrInsert, $valPrev) as $val) {
			$this->resultSecond[] = $val;
		}
		return $this->resultSecond;
	}


}

$task = new positioningElements($arr);


$numInsert = 3;
$arr = $task->getArray();
$arrInsert = $task->getArray($numInsert);

echo "<pre> Array: <br>";
print_r($arr);
echo "<br> ArrayInsert: <br>";
print_r($arrInsert);
echo "<br>Task: <br>";
print_r($task->task($arr));
echo "<br>TaskSecond: <br>";
print_r($task->taskSecond($arrInsert));
echo "</pre>";
