<?php 

/*Разделить заданный массив А(N) на два массива:  
        а) массив положительных и отрицательных элементов;  
        б) массив четных и нечетных элементов.  
*/

	
function task($arr) {

	$result = array();
	foreach ($arr as $key => $value) {
		if($value >= 0) {
			if($value % 2 == 0) {
				$result['positive']['even'][] = $value;
			} else {
				$result['positive']['odd'][] = $value;
			}
		} else {
			if($value % 2 == 0) {
				$result['negative']['even'][] = $value;
			} else {
				$result['negative']['odd'][] = $value;
			}
		} 
	}
	return $result;
}

$arr = array();
for($i = 0; $i < 30; $i++)
	$arr[] = rand(-30, 10);
var_dump($arr);

echo "<pre>";
print_r(task($arr));
echo "</pre>";