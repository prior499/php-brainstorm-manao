<?php 

/*В массиве А(N) все четные элементы заменить максимальным элементом, а нечетные  минимальным. */

function task($arrA) {
	$maxElem = NULL;
	$minElem = NULL;

	foreach ($arrA as $key => $value) {
		if($maxElem < $value || empty($maxElem)) 
			$maxElem = $value;
		if($minElem > $value || empty($minElem)) 
			$minElem = $value;
	}

	foreach ($arrA as $key => $value) {
		if($value % 2 == 0)
			$arrA[$key] = $maxElem;
		else 
			$arrA[$key] = $minElem;
	}
	return $arrA;
}

$arr = array();
for($i = 0; $i < 20; $i++)
	$arr[] = rand(-30, 10);
var_dump($arr);

var_dump(task($arr));