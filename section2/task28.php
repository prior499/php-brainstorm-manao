<?php 
/*
28. �� ������� �(N) ������� ��������, ������� �� ������ ������������ ���������, ���������� ���� ������� ����� k.

*/
function firstMaxElem($arr) {
	$elem = array("key" => 0, "value" => current($arr));
	foreach ($arr as $key => $val) {
		if($val > $elem['value']) {
			$elem = array("key" => $key, "value" => $val);
		}
	}
	return $elem;
}
 
function task($arr, $k) {
	$firstMaxElem = firstMaxElem($arr);
	print_r($firstMaxElem);
	$searchElem = false;
	foreach ($arr as $key => $value) {
		if($searchElem && strlen($value) == $k){
			unset($arr[$key]);
			continue;
		}
		if(!$searchElem && $key === $firstMaxElem['key']) 
			$searchElem = true;
	}
	return $arr;
}

$arr = array();
for($i = 0; $i < 7; $i++)
	$arr[] = rand(1, 1000);
echo "<pre> Array: <br>";
print_r($arr);
echo "</pre>";


$k = 2;
echo "<pre> Result: <br>";
print_r(task($arr, $k));
echo "</pre>";
