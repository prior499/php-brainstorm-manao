<?php 

/*
33. � ������� �(N) ������������� �������� ����������� � ������� �����������, ����� �������� � ������ �������� ������������� ����� � , �������� ��������������� ���������.

*/

class positioningElements
{
	public $arr;

	function __construct($arr) {
		$this->arr = $arr;
	}

	private function getMinPositiveElem() {
		$elem = false;
		foreach ($this->arr as $key => $val) {
			if( $val >= 0 && ($elem === false || $val < $elem) ) {
				$elemKey = $key;
				$elem = $val;
			}
		}
		unset($this->arr[$elemKey]);
		return $elem;
	}

	public function task() {
		$this->result = array();
		foreach ($this->arr as $key => $value) {
			if($value >= 0)
				$this->result[] = $this->getMinPositiveElem();
			else 
				$this->result[] = $value;
		}
		return $this->result;
	}

	public function taskSecond($numInsert) {
		$valuePrev = false;
		$resultSecond = array();
		$insertNum = false;
		foreach ($this->result as $key => $value) {
			if($valuePrev === false) {
				$valuePrev = $value;
			}
			if(!$insertNum && $numInsert > $valuePrev && $numInsert <= $value) {
				$resultSecond[] = $numInsert;
				$insertNum = true;
			}
			$resultSecond[] = $value;
			$valuePrev = $value;
		}
		if(!$insertNum) {
			$resultSecond[] = $numInsert;
		}
		return $resultSecond;
	}


}

$arr = array();
for($i = 0; $i < 7; $i++)
	$arr[] = rand(-10, 10);
echo "<pre> Array: <br>";
print_r($arr);
echo "</pre>";


$numInsert = 6;
$task = new positioningElements($arr);
echo "<pre> sort asc positive nums: <br>";
print_r($task->task());
echo "<br>arr after insert nums $numInsert: <br>";
print_r($task->taskSecond($numInsert));
echo "</pre>";
