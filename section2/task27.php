<?php 

/*
27. � ������ �(N), ������������� �� �����������, ��������   k ���������, �� ������� ��� ������������������.
*/

function maxValue($arr) {
	$maxValue = false;
	foreach ($arr as $val) {
		if($val > $maxValue || !$maxValue) $maxValue = $val;
	}
	return $maxValue;
}
function minValue($arr) {
	$minValue = false;
	foreach ($arr as $val) {
		if($val < $minValue || !$minValue) $minValue = $val;
	}
	return $minValue;
}

function getNumFromArray($numsInclude, $rangeMin = false, $rangeMax = false) {
	$result = array();
	$rangeMinNum = $rangeMin;
	$rangeMaxNum = $rangeMax;
	if( $rangeMin === false) {
		$rangeMinNum = minValue($numsInclude);
		if($rangeMinNum <= $rangeMax)
			$result[] = $rangeMinNum;
	}
	if( $rangeMax === false)
		$rangeMaxNum = maxValue($numsInclude);

	foreach($numsInclude as $val) {
		if($val > $rangeMinNum && $val <= $rangeMaxNum)
			$result[] = $val;
	}
	
	sort($result);
	return $result;
}
 
function task($arr, $k) {

	$numsInclude = array();
	for($i = 0; $i < $k; $i++) 
		$numsInclude[] = rand(-7, 9);
	echo "IncludeNums";
	var_dump($numsInclude);

	$result = array();
	$valPrev = false;

	foreach ($arr as $valA) { 
		foreach( getNumFromArray($numsInclude, $valPrev, $valA) as $valB ) {
			$result[] = $valB;
		}
		$result[] = $valA;
		$valPrev = $valA;
	}

	$valLast = end($result);
	foreach( getNumFromArray($numsInclude, $valLast) as $valB ) {
		$result[] = $valB;
	}

	return $result;
}

$arr = array();
for($i = 0; $i < 7; $i++)
	$arr[] = rand(-7, 9);
sort($arr);
var_dump($arr);

$k = 3;
echo "<pre> Result";
print_r(task($arr, $k));
echo "</pre>";
