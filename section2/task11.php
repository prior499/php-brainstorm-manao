<?php 

/*В массиве А(N) найти первый отрицательный элемент, предшествующий максимальному элементу, и последний положительный элемент, стоящий за минимальным элементом. */

function task($arrA) {


	$maxElem = NULL;
	$minElem = NULL;

	foreach ($arrA as $key => $value) {
		if($maxElem < $value || empty($maxElem)) 
			$maxElem = $value;
		if($minElem > $value || empty($minElem)) 
			$minElem = $value;
	}

	$data = array('elemA' => NULL, 'elemB' => NULL);
	$triggerFirstElem = true;
	foreach ($arrA as $currentElem => $currentValue) {

		if($currentValue == $maxElem && $data['elemA'] == NULL) {
			$triggerFirstElem = false;
		} 
		if($currentValue < 0 && $triggerFirstElem) {
			$data['elemA'] = $currentElem;
			$triggerFirstElem = false;
		}

		if($currentValue == $minElem) {
			$triggerSecondElem = true;
		} 
		if($currentValue > 0 && $triggerSecondElem == true) {
			$data['elemB'] = $currentElem;
		}
	}

	return $data;
}

$arr = array();
for($i = 0; $i < 20; $i++)
	$arr[] = rand(-30, 10);
var_dump($arr);

var_dump(task($arr));