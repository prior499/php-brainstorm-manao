<?php 
$arNum = range(0, 15);
shuffle($arNum);
var_dump($arNum);

$countNumBeforeNegative = 0;
$triggerAfterNegativeNum = false;
$sumOddNumAfterNegative = 0;
foreach ($arNum as $value) {

    if($triggerAfterNegativeNum) {
        if($value % 2 != 0) {
            $sumOddNumAfterNegative += $value;
        }
        if($value < 0) // Если число отрицательное сбрасываем сумму
            $sumOddNumAfterNegative = 0;
    } else {
        if($value < 0) {
            $triggerAfterNegativeNum = true;
            continue;
        }
        $countNumBeforeNegative++;
    }
}

echo "Количество елементов до негативного ".$countNumBeforeNegative;
echo "<br> Сумма нечетных елементов после негативного ".$sumOddNumAfterNegative;