<?php 

/*В  массиве  А(N)  найти  максимальный  среди  четных  элементов  и  минимальный среди нечетных.  */

function task($arrA) {
	$maxEvenElem = NULL;
	$minOddElem = NULL;

	foreach ($arrA as $key => $value) {

		if(($maxEvenElem < $value || empty($maxEvenElem)) && $value % 2 == 0) 
			$maxEvenElem = $value;
		if(($minOddElem > $value || empty($minOddElem)) && $value % 2 != 0) 
			$minOddElem = $value;
	}
	echo "максимальный  среди  четных ".$maxEvenElem;
	echo "<br>минимальный среди нечетных ".$minOddElem;
	return $arrA;
}

$arr = array();
for($i = 0; $i < 20; $i++)
	$arr[] = rand(-30, 10);
var_dump($arr);

task($arr);