<?php 

/*
35. � ������� �(N) ������ ������� ����� 0, 1 ��� 2. �����������   �������� ������� ���, ����� ������� ������������� ��� ����, ����� ��� ������ �, �������, ��� �������.  


*/

class positioningElements
{
	public $arr;
	public $result;

	public function getArray($countElem = 10, $rangeMin = -10, $rangeMax = 10) {
		$arr = array();
		for($i = 0; $i < $countElem; $i++)
			$arr[] = rand($rangeMin, $rangeMax);
		return $arr;
	}

	public function getMaxElem($arr) {
		$maxElem = false;
		foreach ($arr as $val) {
			if($val > $maxElem || $maxElem === false) $maxElem = $val;
		}
		return $maxElem;
	}
	public function getMinElem($arr) {
		$minElem = false;
		foreach ($arr as $val) {
			if($val < $minElem || $minElem === false) $minElem = $val;
		}
		return $minElem;
	}

	public function task($arr) {
		$this->result = array();
		$minElem = $this->getMinElem($arr);
		$maxElem = $this->getMaxElem($arr);
		foreach ($arr as $value) {
			if($value == $minElem)
				$this->result[] = $value;
		}
		foreach ($arr as $value) {
			if($value == $maxElem)
				$this->result[] = $value;
		}
		foreach ($arr as $value) {
			if($value != $maxElem && $value != $minElem)
				$this->result[] = $value;
		}
		return $this->result;
	}


}

$task = new positioningElements($arr);


$numInsert = 3;
$arr = $task->getArray(10, 0, 2);

echo "<pre> Array: <br>";
print_r($arr);
echo "<br>Task: <br>";
print_r($task->task($arr));
echo "</pre>";
