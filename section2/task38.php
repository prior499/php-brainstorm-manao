<?php 

/*
38. В массиве А(N) все отрицательные элементы переставить в начало массива, расположив их по возрастанию, а положительные  в конец, расположив их по убыванию.

*/

class positioningElements
{
	public $arr;
	public $result;

	public function getArray($countElem = 10, $rangeMin = -10, $rangeMax = 10) {
		$arr = array();
		for($i = 0; $i < $countElem; $i++)
			$arr[] = rand($rangeMin, $rangeMax);
		return $arr;
	}

	public function getMaxElem($arr) {
		$maxElem = false;
		foreach ($arr as $key => $val) {
			if($val > $maxElem['value'] || $maxElem === false) 
				$maxElem = array('key' => $key, 'value' => $val);
		}
		return $maxElem;
	}

	public function getMinElem($arr) {
		$minElem = false;
		foreach ($arr as $key => $val) {
			if($val < $minElem['value'] || $minElem === false) 
				$minElem = array('key' => $key, 'value' => $val);
		}
		return $minElem;
	}

	public function getDescArr($arr) {
		$result = array();
		$countElem = count($arr); 
		for ($i = 0; $i < $countElem; $i++) {
			$elem = $this->getMaxElem($arr);
			$result[] = $elem['value'];
			unset( $arr[ $elem['key'] ] );
		}
		return $result;
	}

	public function getAscArr($arr) {
		$result = array();
		$countElem = count($arr); 
		for ($i = 0; $i < $countElem; $i++) {
			$elem = $this->getMinElem($arr);
			$result[] = $elem['value'];
			unset( $arr[ $elem['key'] ] );
		}
		return $result;
	}

	public function task($arr) {
		$this->arr = $arr;
		$this->result = array();
		$arrNegative = array();
		$arrPositive = array();
		foreach ($arr as $value) {
			if($value < 0) 
				$arrNegative[] = $value;
			else
				$arrPositive[] = $value;
		}
		$arrNegativeAsc = $this->getAscArr($arrNegative);
		$arrPositiveDesc = $this->getDescArr($arrPositive);
		$this->result = array_merge($arrNegativeAsc, $arrPositiveDesc);
		return $this->result;
	}

}

$task = new positioningElements($arr);


$numInsert = 3;
$arr = $task->getArray(10, -3, 3);

echo "<pre> Array: <br>";
print_r($arr);
echo "<br>Task: <br>";
print_r($task->task($arr));
echo "</pre>";


