<?php 

/*
39. � ������� �(N) ����������� ���� ����� �� 1 �� 20. ����������� �������� ������� � ������� ������� ������������� �����.

*/

class positioningElements
{
	public $arr;
	public $result;

	public function getArray($countElem = 10, $rangeMin = -10, $rangeMax = 10) {
		$arr = array();
		for($i = 0; $i < $countElem; $i++)
			$arr[] = rand($rangeMin, $rangeMax);
		return $arr;
	}

	public function getMinElem($arr) {
		$minElem = false;
		foreach ($arr as $key => $val) {
			if($val < $minElem['value'] || $minElem === false) 
				$minElem = array('key' => $key, 'value' => $val);
		}
		return $minElem;
	}

	public function getAscArr($arr) {
		$result = array();
		$countElem = count($arr); 
		for ($i = 0; $i < $countElem; $i++) {
			$elem = $this->getMinElem($arr);
			$result[$elem['key']] = $elem['value'];
			unset( $arr[ $elem['key'] ] );
		}
		return $result;
	}

	public function countElems($arr, $elem) {
		$count = 0;
		$countElem = count($arr);
		for ($i = 0; $i < $countElem; $i++) {
			if($arr[$i] == $elem) {
				$count++;
			}
		}
		return $count;
	}


	public function task($arr) {
		$this->arr = $arr;
		$this->result = array();
		$arrElems = array();
		$countedElem = array();
		foreach ($arr as $value) {
			if(!in_array($value, $countedElem)) {
				$arrElems[$value] = $this->countElems($arr, $value);
				$countedElem[] = $value;
			}
		}
		$arrAscElems = $this->getAscArr($arrElems);

		$countElems = count($arrAscElems);
		for ($i = 0; $i < $countElems; $i++) {
			$qtyElem = current($arrAscElems);
			while($qtyElem > 1) {
				$this->result[] = key($arrAscElems);
				$qtyElem--;
			}
			$this->result[] = key($arrAscElems);
			next($arrAscElems);
			$count++;
		}
		return $this->result;
	}

}

$task = new positioningElements($arr);


$numInsert = 3;
$arr = $task->getArray(10, 1, 20);

echo "<pre> Array: <br>";
print_r($arr);
echo "<br>Task: <br>";
print_r($task->task($arr));
echo "</pre>";


