<?php 

function task($arrA) {
	$firstEvenElem = false;
	$triggerFirstEvenElem = true;
	$lastOddElem = false;
	foreach ($arrA as $key => $value) {
		if($value >= 0 && $triggerFirstEvenElem) {  // выбор первого элемента и запретить выбор
			$firstEvenElem = array('key' => $key, 'val' => $value);
			$triggerFirstEvenElem = false;
		} else {
			$lastOddElem =  array('key' => $key, 'val' => $value);
		}
	}
	$arrA[$firstEvenElem['key']] = $lastOddElem['val'];
	$arrA[$lastOddElem['key']] = $firstEvenElem['val'];
	return $arrA;
}

$arr = range(-2, 5);
shuffle($arr);
var_dump($arr);

var_dump(task($arr));