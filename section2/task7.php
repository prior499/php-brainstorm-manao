<?php 

/*В  массиве  А(N)  найти  номер  элемента,  сумма  которого  со  следующим за ним элементом максимальна, и номер элемента, сумма которого с предыдущим элементом минимальна.  */

function task($arrA) {
	$data = array('maxSum' => NULL, 'minSum' => NULL, 
		'firstElemWitchMaxSum' => '', 'secondElemWithMinSum' => '', 
		'oldElem' => '', 'oldValue' => NULL);
	foreach ($arrA as $currentElem => $currentValue) {
		if($data['oldValue'] == NULL) {  // выбор первого элемента
			$data['oldElem'] = $currentElem;
			$data['oldValue'] = $currentValue;
			continue;
		} 

		$testSum = $data['oldValue'] + $currentValue;

		if(!isset($data['maxSum'])) {
			$data['maxSum'] = $testSum;
			$data['firstElemWitchMaxSum'] = $data['oldElem'];
		}
		if(!isset($data['minSum'])) {
			$data['secondElemWithMinSum'] = $currentElem; 
			$data['minSum'] = $testSum;
		}

		if($data['maxSum'] < $testSum) {
			$data['maxSum'] = $testSum;
			$data['firstElemWitchMaxSum'] = $data['oldElem']; 
		} 
		if($data['minSum'] > $testSum) {
			$data['minSum'] = $testSum;
			$data['secondElemWithMinSum'] = $currentElem; 
		} 

		$data['oldElem'] = $currentElem;
		$data['oldValue'] = $currentValue;
	}
	
	echo 'номер  элемента,  сумма  которого  со  следующим за ним элементом максимальна '.$data['firstElemWitchMaxSum'];
	echo '<br>номер элемента, сумма которого с предыдущим элементом минимальна '.$data['secondElemWithMinSum'];
	
	return $data;
}

// $arr = array(1,3,10,4);
for($i = 0; $i < 10; $i++)
	$arr[] = rand(1, 10);
var_dump($arr);

var_dump(task($arr));