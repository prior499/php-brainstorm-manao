<?php 

/*
26. � ������ �(N) �������� ������������ ������� ����� ������� ������� �������������� ��������.

*/

function isEven($num) {
	return $num % 2 == 0 ? true : false;
}
function isPositive($num) {
	return $num >= 0 ? true : false;
}
function maxElem($arr) {
	$maxElem = false;
	foreach ($arr as $val) {
		if($val > $maxElem || !$maxElem) $maxElem = $val;
	}
	return $maxElem;
}

function task($arr, $k) {
	$maxElem = maxElem($arr);
	$result = array();
	foreach ($arr as $key => $val) {
		$result[] = $val;
		if(isEven($val) && !isPositive($val)) {
			$result[] = $maxElem;
		}
	}
	return $result;
}

$arr = array();
for($i = 0; $i < 7; $i++)
	$arr[] = rand(-7, 9);
var_dump($arr);

$k = 3;
echo "<pre>";
print_r(task($arr, $k));
echo "</pre>";