<?php 

/*
32. � ������� �(N) ����������� ��� ������ �������� � ������� �����������, � �������� � ������� ��������.

*/

class positioningElements
{
	public $arr;

	function __construct($arr) {
		$this->arr = $arr;
	}

	private function isEven($num) {
		return $num % 2 == 0 ? true : false;
	}

	private function getMinEvenElem() {
		$elem = false;
		foreach ($this->arr as $key => $val) {
			if( $this->isEven($val) && ($elem === false || $val < $elem) ) {
				$elemKey = $key;
				$elem = $val;
			}
		}
		unset($this->arr[$elemKey]);
		return $elem;
	}
	private function getMaxOddElem() {
		$elem = false;
		foreach ($this->arr as $key => $val) {
			if( !$this->isEven($val) && ($elem === false || $val > $elem) ) {
	 			$elemKey = $key;
	 			$elem = $val;
	 		}
	 	}
	 	unset($this->arr[$elemKey]);
		return $elem;
	}

	public function task() {
		$this->result = array();
		foreach ($this->arr as $key => $value) {
			if( $this->isEven($value) ) {
				$this->result[] = $this->getMinEvenElem();
			} else {
				$this->result[] = $this->getMaxOddElem();
			}
		}
		return $this->result;
	}

}

$arr = array();
for($i = 0; $i < 7; $i++)
	$arr[] = rand(1, 1000);
echo "<pre> Array: <br>";
print_r($arr);
echo "</pre>";

$task = new positioningElements($arr);
echo "<pre> class: <br>";
print_r($task->task());
echo "</pre>";



// $k = 3;
// echo "<pre> Result: <br>";
// print_r(task($arr, $k));
// echo "</pre>";
