<?php 

/*В массиве А(N) найти максимальный и минимальный элементы и переставить их местами.*/

function task($arrA) {
	$minElem = array();
	$maxElem = array();
	foreach ($arrA as $key => $value) { 
		if(empty($minElem) && empty($maxElem)) { // если пустые переменные заполняем
			$minElem = array('key' => $key, 'val' => $value);
			$maxElem = array('key' => $key, 'val' => $value);
		}
		if($minElem['val'] > $value) {  
			$minElem = array('key' => $key, 'val' => $value);
		} 
		if($maxElem['val'] < $value) {  
			$maxElem = array('key' => $key, 'val' => $value);
		} 
	}
	$arrA[$minElem['key']] = $maxElem['val']; // меняем местами
	$arrA[$maxElem['key']] = $minElem['val'];
	return $arrA;
}

$arr = range(-10, 5);
shuffle($arr);
var_dump($arr);

var_dump(task($arr));