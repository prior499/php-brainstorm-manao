<?php 

/*
25. � ������ �(N) �������� ����� ������� ������������� �������� k   ����������� ��������� �������.
*/

function task($arr, $k) {
	$firstMaxElem = false;
	$minElem = false;
	foreach ($arr as $ka => $a) {
		if(!$firstMaxElem || $a > $firstMaxElem) {
			$firstMaxElemKey = $ka;
			$firstMaxElem = $a;
		}
		if(!$minElem || $a < $minElem) {
			$minElem = $a;
		}
	}

	$arrFirst = array_slice($arr, 0, $firstMaxElemKey+1);
	$i = 0;
	while ($i < $k) {
		$arrFirst[] = $minElem;
		$i++;
	}
	$arrEnd = array_slice($arr, $firstMaxElemKey+1, count($arr)-1);
    $result = array_merge($arrFirst, $arrEnd);

	return $result;
}

$arr = array();
for($i = 0; $i < 3; $i++)
	$arr[] = rand(1, 5);
var_dump($arr);

$k = 3;
echo "<pre>";
print_r(task($arr, $k));
echo "</pre>";