<?php 

/*
36. � ������� �(N) ������������� �������� ����������� � ����� ������� � ����������� � ������� ��������.


*/

class positioningElements
{
	public $arr;
	public $result;

	public function getArray($countElem = 10, $rangeMin = -10, $rangeMax = 10) {
		$arr = array();
		for($i = 0; $i < $countElem; $i++)
			$arr[] = rand($rangeMin, $rangeMax);
		return $arr;
	}
	
	public function getMaxElem($arr) {
		$maxElem = false;
		foreach ($arr as $key => $val) {
			if($val > $maxElem['value'] || $maxElem === false) 
				$maxElem = array('key' => $key, 'value' => $val);
		}
		return $maxElem;
	}

	public function getDescArr($arr) {
		$result = array();
		$countElem = count($arr); 
		for ($i = 0; $i < $countElem; $i++) {
			$elem = $this->getMaxElem($arr);
			$result[] = $elem['value'];
			unset( $arr[ $elem['key'] ] );
		}
		return $result;
	}

	public function task($arr) {
		$this->arr = $arr;
		$this->result = array();
		foreach ($arr as $value) {
			if($value >= 0) 
				$arrPositive[] = $value;
			else
				$this->result[] = $value;
		}
		$arrPositiveDesc = $this->getDescArr($arrPositive);
		$this->result = array_merge($this->result, $arrPositiveDesc);
		return $this->result;
	}


}

$task = new positioningElements($arr);


$numInsert = 3;
$arr = $task->getArray(10);

echo "<pre> Array: <br>";
print_r($arr);
echo "<br>Task: <br>";
print_r($task->task($arr));
echo "</pre>";


