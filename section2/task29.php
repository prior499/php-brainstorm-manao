<?php 

/*
29. �� ������� �(N) ������� ��� ��������, ������� ����� ������ ����������� � ��������� ������������ ����������.

*/
function firstMinElemKey($arr) {
	$elem = false;
	foreach ($arr as $key => $val) {
		if($elem === false || $val < $elem) {
			$elemKey = $key;
			$elem = $val;
		}
	}
	return $elemKey;
}
function lastMaxElemKey($arr) {
	$elem = false;
	foreach ($arr as $key => $val) {
		if($elem === false || $val > $elem) {
			$elemKey = $key;
			$elem = $val;
		}
	}
	return $elemKey;
}
function task($arr, $k) {
	$firstMinElemKey = firstMinElemKey($arr);
	$lastMaxElemKey = lastMaxElemKey($arr);
	$unsetElem = false;
	foreach ($arr as $key => $value) {
		if($firstMinElemKey == $key){
			$unsetElem = true;
			continue;
		}
		if($lastMaxElemKey == $key) 
			break;
		if($unsetElem)
			unset($arr[$key]);
	}
	return $arr;
}

$arr = array();
for($i = 0; $i < 7; $i++)
	$arr[] = rand(1, 1000);
echo "<pre> Array: <br>";
print_r($arr);
echo "</pre>";


$k = 3;
echo "<pre> Result: <br>";
print_r(task($arr, $k));
echo "</pre>";
