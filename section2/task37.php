<?php 

/*
37. � ������� �(N) ������� �������� ����������� � ������ �������, � ��������� ����������� � ������� �����������.

*/

class positioningElements
{
	public $arr;
	public $result;

	public function getArray($countElem = 10, $rangeMin = -10, $rangeMax = 10) {
		$arr = array();
		for($i = 0; $i < $countElem; $i++)
			$arr[] = rand($rangeMin, $rangeMax);
		return $arr;
	}

	public function getMinElem($arr) {
		$maxElem = false;
		foreach ($arr as $key => $val) {
			if($val < $maxElem['value'] || $maxElem === false) 
				$maxElem = array('key' => $key, 'value' => $val);
		}
		return $maxElem;
	}

	public function getAscArr($arr) {
		$result = array();
		$countElem = count($arr); 
		for ($i = 0; $i < $countElem; $i++) {
			$elem = $this->getMinElem($arr);
			$result[] = $elem['value'];
			unset( $arr[ $elem['key'] ] );
		}
		return $result;
	}

	public function task($arr) {
		$this->arr = $arr;
		$this->result = array();
		$arrZero = array();
		foreach ($arr as $value) {
			if($value == 0) 
				$arrZero[] = $value;
			else
				$this->result[] = $value;
		}
		$this->result = $this->getAscArr($this->result);
		$this->result = array_merge($arrZero, $this->result);
		return $this->result;
	}

}

$task = new positioningElements($arr);
$arr = $task->getArray(10, -1, 5);
echo "<pre> Array: <br>";
print_r($arr);
echo "<br>Task: <br>";
print_r($task->task($arr));
echo "</pre>";


