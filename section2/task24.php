<?php 

/*
24. �� ���� �������� ������� �(N), ������ ��������� �� �����, ������� ����� ������� � �������� ������� ��� ������ � �����.

*/

class positioningElements
{
	public $arr;
	
	public function getMaxElem($arr) {
		$maxElem = false;
		foreach ($arr as $key => $val) {
			if($val > $maxElem['value'] || $maxElem === false) 
				$maxElem = array('key' => $key, 'value' => $val);
		}
		return $maxElem;
	}

	public function getArray($countElem = 10, $rangeMin = -10, $rangeMax = 10) {
		$arr = array();
		for($i = 0; $i < $countElem; $i++)
			$arr[] = rand($rangeMin, $rangeMax);
		return $arr;
	}

	public function task() {
		$countElems = count($this->arr);
		$firstZeroKey = false;
		$lastZeroKey = false;
		$zeroSelect = false;
		$zeroKeys = array();
		$zeroLinks = array();
		$zeroCount = 0;
		for ($i = 0; $i < $countElems; $i++) {
			$elem = current($this->arr);
			$zeroKey = key($this->arr);
			if($elem === 0 && !$zeroSelect) {
				$firstZeroKey = $zeroKey;
				$zeroSelect = true;
			} 
			if($elem === 0 && $zeroSelect) {
				$zeroCount++;
			}
			$elemNext = next($this->arr);
			if($elemNext !== 0 && $zeroSelect){
				$lastZeroKey = $zeroKey;
				$zeroSelect = false;
				$zeroKeys[$i] = array('FIRST_MAX_ZERO_INDEX' => $firstZeroKey, 'LAST_MAX_ZERO_INDEX' => $lastZeroKey);
				$zeroLinks[$i] = $zeroCount;
				$zeroCount = 0;
			} 
		}

		$maxZeroLink = $this->getMaxElem($zeroLinks);
		return $zeroKeys[$maxZeroLink['key']];
	}

}

$task = new positioningElements($arr);

$task->arr = $task->getArray(10, 0, 1);

echo "<pre> Array 1: <br>";
print_r($task->arr);
echo "<br>Task: <br>";
print_r($task->task());
echo "</pre>";


