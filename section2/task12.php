<?php 

/*В массиве А(N) найти наибольший из всех отрицательных элементов и наименьший из всех положительных. */

function task($arrA) {
	$maxElem = NULL;
	$minElem = NULL;

	foreach ($arrA as $value) {
		if(($maxElem < $value || empty($maxElem)) && $value < 0) 
			$maxElem = $value;
		if(($minElem > $value || empty($minElem)) && $value > 0) 
			$minElem = $value;
	}

	return array('MaxNegativeElem' => $maxElem, 'MinPositiveElem' => $minElem);
}

$arr = array();
for($i = 0; $i < 20; $i++)
	$arr[] = rand(-30, 10);
var_dump($arr);

var_dump(task($arr));