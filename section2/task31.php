<?php 

/*
31. ��������� �������� ���������� � ���� ��� ����������� � �������� ����� x (������������ �������� � �������� ).
*/

class polonomialValue
{
	public $coefficient;
	public $x;

	function calculatePolonomial()
	{
		$result = 0;
		$countCoeff = count($this->coefficient);
		$maxPow = $countCoeff-1;
		for($i = 0; $i < $countCoeff; $i++, $maxPow--) {
			$result += $this->coefficient[$i]*($this->x**$maxPow);
		}
		return $result;
	}
	function calculateDerivative()
	{
		$result = array();
		$countCoeff = count($this->coefficient)-1;
		$maxPow = $countCoeff;
		for($i = 0; $i < $countCoeff; $i++, $maxPow--) {
			$result[] = $this->coefficient[$i]*$maxPow;
		}
		$this->coefficient = $result;
		return $result;
	}

}

$task = new polonomialValue($arr);

$task->coefficient = array(2, 3, 2, 4);
$task->x = 2;

echo "<pre>";

$i = 0;
while ($task->coefficient) {
	echo "<br>Polonomial $i: <br>";
	print_r($task->calculatePolonomial());
	echo "<br>";
	$task->calculateDerivative();
	$i++;
}
echo "</pre>";
