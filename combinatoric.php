<?php 
class combinatoricError extends Exception
{
    protected $dirFile = "errors.txt";
    public function logError($e)
    {
        $getErr = file_get_contents($this->dirFile);
        $file = fopen($this->dirFile, "w");
        $date = date('Y-m-d H:i:s');
        $getErr .= $date . "\n". $e . "\n\n";
        fwrite($file, $getErr);
        fclose($file);
    }
    public function getErrors()
    {
        echo "</br> Список ошибок:</br>";
        if (file_exists($this->dirFile))
            echo "<pre>" . file_get_contents($this->dirFile) . "</pre>";
        else
            echo "История чиста";
    }
    public function displayError($e)
    {
        echo "Oшибка: </br>" . $e;
    }
}

class combinatoric
{

    public $element;
    public $lenghtCombination;
    public $combinations = array();

    public function __construct($element, $lenghtCombination) {
        $this->element = $element;
        $this->lenghtCombination = $lenghtCombination;
        // проверка входных данных
        $this->checkElement($this->element);
        $this->checkLenght($this->element, $this->lenghtCombination);
    }

    public function getCountCombinations()
    {
        $countElement = count($this->element);
        $result = $this->factorial($countElement) / $this->factorial($countElement - $this->lenghtCombination);
        return $result;
    }
    protected function factorial ($num) {
         return ($num < 2 ? 1 : $num * $this->factorial($num-1));
    }

    protected function checkElement($element) {
        if(!is_string($element))  
            throw new combinatoricError('Входное значение должно быть строкой.');
        $this->element = preg_split('//u', $element, null, PREG_SPLIT_NO_EMPTY);   
    }
    protected function checkLenght($element, $lenghtCombination) {
        if(count($this->element) <= $lenghtCombination)
            throw new combinatoricError('Длина входного значения должно быть меньше количества элементов.');
    }

    public function startCombine() {
        $this->combineMechanism($this->element, $this->lenghtCombination);
        return $this->combinations;
    }

    protected function combineMechanism($element, $lenghtCombination = 2, $combination = '') {
        
        foreach ($element as $key => $testSymbol) {
            
            //убрать текущий символ среди элементов
            $tempElement = $element;
            unset($tempElement[$key]); 

            // сбор комбинации
            $currentCombination = $combination; 
            $currentCombination .= $testSymbol;

            $lenghtCombination--;

            if($lenghtCombination > 0) { // Если комбинация не завершена то воспроизвести комбинацию снова, без уже имеющихся обьектов
                if($this->combineMechanism($tempElement, $lenghtCombination, $currentCombination)) {
                    $lenghtCombination++; 
                    continue;
                }
            } else {
                unset($tempElement[$key]);
                $this->combinations[] = $currentCombination; 
            }
        }
        return true;
    }
}

$element = 'abcабц'; //only string < count element for combination
$lenghtCombination = 3; 

try {
    $combinatoric = new combinatoric($element, $lenghtCombination);
    var_dump($combinatoric->startCombine()); 
    echo $combinatoric->getCountCombinations(); // сверка результата работы.

} catch (combinatoricError $e) {
    // $e->logError($e); // запись ошибок
    $e->displayError($e); // отображение ошибки
}

// $listError = new combinatoricError(); // Получение ошибок
// $listError->getErrors();
